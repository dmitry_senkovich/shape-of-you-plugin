package com.proop.lab4.plugintest;

import com.proop.lab4.json.JsonUnmarshaller;
import com.proop.lab4.shape.Shape;

import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.Graphics2D;
import java.io.File;
import java.io.IOException;
import java.net.URL;
import java.util.List;
import java.util.NoSuchElementException;
import java.util.Optional;

import javax.swing.JPanel;
import javax.swing.Timer;
import javax.swing.event.AncestorEvent;
import javax.swing.event.AncestorListener;

import javafx.embed.swing.JFXPanel;
import javafx.scene.media.Media;
import javafx.scene.media.MediaPlayer;

public class ShapeOfYouForm extends JPanel {

    private ShapeOfYou shapeOfYou;
    private JsonUnmarshaller jsonUnmarshaller = new JsonUnmarshaller();

    private int currentTick = 1;
    private Timer timer;
    private MediaPlayer mediaPlayer;

    public ShapeOfYouForm(File file) throws IOException {
        initComponents();
        loadShapeOfYou(file);
        startAnimation();
        playShapeOfYou();
    }

    private void loadShapeOfYou(File file) throws IOException {
        try {
            List<Shape> shapes = jsonUnmarshaller.read(file);
            Optional<Shape> optionalShapeOfYou = shapes.stream().filter(shape -> shape instanceof ShapeOfYou).findFirst();
            if (!optionalShapeOfYou.isPresent()) {
                throw new NoSuchElementException("No shapes of you found in file: " + file.getName());
            }
            shapeOfYou = (ShapeOfYou) optionalShapeOfYou.get();
        } catch (IOException | NoSuchElementException e) {
            e.printStackTrace();
            System.out.println("No shapes of you found in file: " + file.getName());
            throw e;
        }
    }

    private void startAnimation() {
        ShapeOfYouForm shapeOfYouForm = this;
        timer = new Timer(50, e -> {
            shapeOfYouForm.repaint();
        });
        timer.start();
    }

    private void stopAnimation() {
        timer.stop();
    }

    private void playShapeOfYou() {
        try {
            // couldn't find any other way to make it work
            new JFXPanel();
            URL file = this.getClass().getClassLoader().getResource("ed_sheeran_shape_of_you.wav");
            Media media = new Media(file.toString());
            mediaPlayer = new MediaPlayer(media);
            mediaPlayer.play();
        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("Failed to play shape of you");
        }
    }

    private void stopPlayingShapeOfYou() {
        mediaPlayer.stop();
    }

    @Override
    public void paintComponent(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.clearRect(0, 0, 250, 250);
        int w2 = getWidth() / 2;
        int h2 = getHeight() / 2;
        g2d.rotate((currentTick - 32)*Math.PI / 16, w2, h2);
        currentTick = (currentTick + 1)%48 + 1;
        shapeOfYou.getPoints().forEach(point -> {
            g2d.drawOval(point.getX(), point.getY(), 3, 3);
        });
    }

    private void initComponents() {
        setLayout(null);
        setPreferredSize(new Dimension(250, 250));
        addAncestorListener(new AncestorListener() {
            @Override
            public void ancestorAdded(AncestorEvent event) {}

            @Override
            public void ancestorRemoved(AncestorEvent event) {
                stopAnimation();
                stopPlayingShapeOfYou();
            }

            @Override
            public void ancestorMoved(AncestorEvent event) {}
        });
    }

}
