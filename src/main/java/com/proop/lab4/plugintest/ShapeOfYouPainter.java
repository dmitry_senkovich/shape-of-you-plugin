package com.proop.lab4.plugintest;

import com.proop.lab4.painter.ShapePainter;

import java.util.stream.Collectors;
import java.util.stream.Stream;

public class ShapeOfYouPainter implements ShapePainter<ShapeOfYou> {

    private static final int MAX_COORDINATES_TO_PRINT = 6;

    @Override
    public String paint(Integer[] coordinates) {
        int coordinatesToPrint = coordinates.length > MAX_COORDINATES_TO_PRINT ? MAX_COORDINATES_TO_PRINT : coordinates.length;
        String back = coordinates.length > MAX_COORDINATES_TO_PRINT ? "..." : "";
        return "shapeOfYou: " +
                Stream.of(coordinates).limit(coordinatesToPrint).map(Object::toString).collect(Collectors.joining(", ")) +
                back;
}

}
