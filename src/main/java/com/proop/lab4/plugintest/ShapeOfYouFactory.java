package com.proop.lab4.plugintest;

import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.shape.factory.ShapeFactory;

import java.util.List;

public class ShapeOfYouFactory implements ShapeFactory<ShapeOfYou> {

    @Override
    public ShapeOfYou createShape(List<Point> points, List<Line> lines) {
        return new ShapeOfYou(points);
    }

}
