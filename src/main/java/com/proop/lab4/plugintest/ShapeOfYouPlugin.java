package com.proop.lab4.plugintest;

import com.proop.lab4.plugin.ShapeButton;
import com.proop.lab4.plugin.ShapeMenu;
import com.proop.lab4.plugin.ShapePlugin;

import java.util.List;

import static java.util.Arrays.asList;

public class ShapeOfYouPlugin implements ShapePlugin {

    @Override
    public List<Class<? extends ShapeButton>> getShapeButtons() {
        return asList(ShapeOfYouButton.class);
    }

    @Override
    public List<Class<? extends ShapeMenu>> getShapeMenus() {
        return asList(ShapeOfYouMenu.class);
    }

}
