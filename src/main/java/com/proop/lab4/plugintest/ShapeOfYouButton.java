package com.proop.lab4.plugintest;

import com.proop.lab4.plugin.ShapeButton;
import com.proop.lab4.shape.Shape;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.List;

import javax.swing.JOptionPane;

public class ShapeOfYouButton extends ShapeButton {

    private List<Shape> shapes;

    public ShapeOfYouButton() {
        setText("Count Shape of You");
        addMouseListener(new MouseAdapter() {
            @Override
            public void mousePressed(MouseEvent e) {
                countShapesOfYouButtonMousePressed();
            }
        });
    }

    @Override
    public void setShapes(List<Shape> shapes) {
        this.shapes = shapes;
    }

    @Override
    public void setShape(Shape shape) {}

    public void countShapesOfYouButtonMousePressed() {
        long shapesOfYouCount = shapes.stream().filter(shape -> shape instanceof ShapeOfYou).count();
        JOptionPane.showMessageDialog(this, String.format("There are %d shapes of you in the collection", shapesOfYouCount));
    }

}
