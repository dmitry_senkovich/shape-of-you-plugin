package com.proop.lab4.plugintest;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.proop.lab4.asset.Line;
import com.proop.lab4.asset.Point;
import com.proop.lab4.shape.Shape;

import java.util.List;
import java.util.stream.Stream;

import static java.util.Collections.emptyList;

public class ShapeOfYou implements Shape {

    public static final String SHAPE_NAME = "shapeOfYou";

    List<Point> points;

    @JsonCreator
    public ShapeOfYou(@JsonProperty("points") List<Point> points) {
        this.points = points;
    }

    @Override
    public Integer[] getCoordinates() {
        return points.stream()
                .flatMap(point -> Stream.of(new Integer[]{ point.getX(), point.getY() })).toArray(Integer[]::new);
    }

    @Override
    public String getShapeName() {
        return SHAPE_NAME;
    }

    @Override
    public List<Point> getPoints() {
        return points;
    }

    @Override
    public void setPoints(List<Point> points) {
        this.points = points;
    }

    @Override
    public List<Line> getLines() {
        return emptyList();
    }

    @Override
    public void setLines(List<Line> list) {}

}
